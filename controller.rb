require './helper'
require './view'
require './model'
require './config'
require './order_builder'

# Controller for the app

class GoCliController

	def initialize
		# Get variable username and options first
		@options = get_arguments
		# Initialize exit
		@exit = false
	end

	# Generate application first
	def generate_app
		# Initialize default
		size = 20
		coor_x = Random.rand(20)
		coor_y = Random.rand(20)
		drivers = []
		# Check options key
		if @options.key?(:file)
			results = read_input_file(@options[:file])
			size = results[0]
			coor_x = results[1][0]
			coor_y = results[1][1]
			drivers = results[2]
		else 
			if @options.key?(:size)
				size = @options[:size].to_i
				coor_x = Random.rand(size)
				coor_y = Random.rand(size)
			end
			if @options.key?(:x)
				coor_x = @options[:x].to_i
			end
			if @options.key?(:y) 
				coor_y = @options[:y].to_i
			end
		end
		# Create user and map
		@map = Map.new(size)
		@user = User.new(read_user_file, coor_x, coor_y)
		@map.set_symbol_to_map(USER_SYMBOL, @user.x, @user.y)
		# Create drivers
		# Check if random or fixed
		# Fixed if read from file
		# Random if doesn't read from file
		if drivers.length == 0
			generate_random_driver(size)
		else
			generate_fixed_driver(drivers)
		end
		# Create new view
		@view = GoCliView.new
	end

	def generate_random_driver(size)
		# Create random driver
		@drivers = []
		for i in 1..5 do
			username = "Driver-" + i.to_s
			while true do
				coor_x = Random.rand(size)
				coor_y = Random.rand(size)
				# If not occupied create new driver
				if not @map.is_grid_occupied(coor_x, coor_y)
					@drivers.push(Driver.new(username, coor_x, coor_y))
					@map.set_symbol_to_map(DRIVER_SYMBOL, coor_x, coor_y)
					break
				end
			end
		end
	end

	def generate_fixed_driver(drivers)
		@drivers = []
		drivers.each do |driver|
			username = "Driver-" + driver[0]
			coor_x = driver[1]
			coor_y = driver[2]
			@drivers.push(Driver.new(username, coor_x, coor_y))
			@map.set_symbol_to_map(DRIVER_SYMBOL, coor_x, coor_y)
		end
	end

	def create_order
		# Intialize
		order = OrderBuilder.new
		# Set user
		order.set_user(@user.username)
		# Set start and end location
		order.set_start_location(@user.x, @user.y)
		order.set_end_location(@map)
		# Get nearest driver
		order.get_nearest_driver(@user, @drivers)
		# Get route
		order.get_route
		# Set price
		order.set_price
		# Show order
		@view.print_order_confirmation(order.return_order)
		# Get input and check the choice
		input = gets.chomp
		if input == "1"
			save_order(order.return_order)
			@view.print_order_saved
			@map.remove_symbol(@user.x, @user.y)
			@user.x = order.return_order.end_loc[:x]
			@user.y = order.return_order.end_loc[:y]
			@map.set_symbol_to_map(USER_SYMBOL, @user.x, @user.y)
		else
			@view.print_order_canceled
		end
	end

	# Get order history user
	def get_user_order_files
		files = []
		username = @user.username.downcase.split(" ").join("")
		Dir.glob("order_history/*.txt") do |file|
			if file.include? username
				files.push(file)
			end
		end
		files
	end

	def generate_orders(files)
		orders = []
		files.each do |file|
			order = OrderBuilder.create_order_from_file(file)
			orders.push(order)
		end
		orders
	end

	# Saving order from user
	def save_order(order)
		time = Time.now
		username = order.user.downcase.split(" ").join("")
		number = get_user_order_files.length + 1
		# Naming rule
		filename = "%d-%s-%02d%02d%d-%02d%02d%02d.txt" % [number, username, time.day, time.month, time.year, time.hour, time.min, time.sec]
		# Begin creating file
		begin
			File.open("order_history/" + filename, "w") do |file|
				file.puts("User: " + order.user)
				file.puts("User_location: " + "(" + order.start_loc[:x].to_s + ", " + order.start_loc[:y].to_s + ")")
				file.puts("Destination_location: " + "(" + order.end_loc[:x].to_s + ", " + order.end_loc[:y].to_s + ")")
				file.puts("Driver: " + order.driver)
				file.puts("Driver_location: " + "(" + order.driver_loc[:x].to_s + ", " + order.driver_loc[:y].to_s + ")")
				file.puts("Route: " + order.route.join(" -> "))
				file.puts("Price: " + order.price.to_s)
				file.puts("Timestamp: %02d-%02d-%d %02d:%02d" % [time.day, time.month, time.year, time.hour, time.min])
			end
		rescue IOError => e
			puts "#{e}"
		end
	end

	# Main loop in here
	def running_app
		# Generate the app first
		generate_app
		# Show the opening
		@view.timedown
		@view.print_logo(@user.username)
		# Show user choices
		show_choices
	end

	# Part of mainloop
	def show_choices
		while not @exit do
			# Show choices
			@view.print_choices
			input = gets.chomp

			if input == "1" or input.downcase == "show map" 
				@view.print_logo(@user.username)
				@view.print_map(@map.map)
			elsif input == "2" or input.downcase == "order go-ride"
				create_order
			elsif input == "3" or input.downcase == "view history"
				files = get_user_order_files.sort_by { |file| file}
				orders = generate_orders(files)
				@view.print_order_history(orders)
			elsif input == "4" or input.downcase == "exit"
				@exit = true
			else
				@view.print_wrong_choice
			end

			if not @exit
				@view.print_continue
				@view.print_logo(@user.username)
			end
		end
		@view.print_goodbye
	end

end