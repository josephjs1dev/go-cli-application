# Model of the app

# Grid Model
class Grid

	attr_accessor :occupied
	attr_accessor :symbol

	def initialize
		@occupied = false
		@symbol = " "
	end
end

# Map Model
class Map

	attr_accessor :size
	attr_accessor :map

	def initialize(size)
		@size = size
		@map = Array.new(size) {Array.new(size) {|i| i = Grid.new}}
	end

	def set_symbol_to_map(symbol, x, y)
		@map[y][x].symbol = symbol
		@map[y][x].occupied = true
	end

	def remove_symbol(x, y)
		@map[y][x].symbol = " "
		@map[y][x].occupied = false
	end

	def is_grid_occupied(x, y)
		@map[y][x].occupied
	end
end

# User model
class User

	attr_accessor :username
	attr_accessor :x
	attr_accessor :y

	def initialize(username, loc_x, loc_y)
		@username = username
		@x = loc_x
		@y = loc_y
	end
end

# Driver model
class Driver

	attr_accessor :username
	attr_accessor :x
	attr_accessor :y

	def initialize(driver_name, loc_x, loc_y)
		@username = driver_name
		@x = loc_x
		@y = loc_y
	end
end

# Order model
class Order

	attr_accessor :start_loc
	attr_accessor :end_loc
	attr_accessor :driver_loc
	attr_accessor :price
	attr_accessor :user
	attr_accessor :driver
	attr_accessor :route

end
